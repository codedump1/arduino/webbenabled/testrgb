#include <Arduino.h>
#include <AllThingsTalk_WiFi.h> // Where the magic happens
#include <WiFiNINA.h>           // This is required only for the purpose of loading the library below.
#include <utility/wifi_drv.h>   // Exposes MKR1010's underlying functions to control the RGB LED

auto wifiCreds   = WifiCredentials("dlink", "05480548");   // Your WiFi Network Name and Password
auto deviceCreds = DeviceConfig("8gBnhd8nxHJFeTG4BaorbdyV", "maker:4ORZNlFcMxo3W1VeVoE29Z5Xl1KW0mo6sgMUyyA");       // Go to AllThingsTalk Maker > Devices > Your Device > Settings > Authentication to get your Device ID and Token
auto device      = Device(wifiCreds, deviceCreds);                // Create "device" object
char* actuator   = "rgbActuator";                                 // Name of the asset on AllThingsTalk
int r,g,b;                                                        // RGB values will be held in these variables

void setup() {
  Serial.begin(115200);                 // Baud rate for Serial output (debug), but you can define any baud rate you want
  WiFiDrv::pinMode(25, OUTPUT);         // Initialize the Green LED pin on the MKR1010 board
  WiFiDrv::pinMode(26, OUTPUT);         // Initialize the Red LED pin on the MKR1010 board
  WiFiDrv::pinMode(27, OUTPUT);         // Initialize the Blue LED pin on the MKR1010 board
  device.debugPort(Serial);             // Set AllThingsTalk library to output its debug to "Serial"
  device.wifiSignalReporting(true);     // Enable AllThingsTalk WiFi SDK's feature that sends NodeMCU's WiFi Signal Strength to your AllThingsTalk Maker
  device.setActuationCallback(actuator, rgb); // Add an actuation callback for asset defined above and run function "rgb" once command is received
  device.init();                        // Initialize WiFi and AllThingsTalk
}

void rgb(String value) {                // Function that will be called when you pick a color on AllThingsTalk
  Serial.println("RGB Color Changed!"); // Outputs to Serial
  // Parses the received RGB information into three separate variables (R, G, B)
  long hexColor = (long) strtol(&value[1], NULL, 16);
  r = hexColor >> 16;
  g = hexColor >> 8 & 0xFF;
  b = hexColor & 0xFF;
  WiFiDrv::analogWrite(25, g);          // Set the Green LED to received value
  WiFiDrv::analogWrite(26, r);          // Set the Red LED to received value
  WiFiDrv::analogWrite(27, b);          // Set the Blue LED to received value
}

void loop() {                           // Runs as long as the device in on
  device.loop();                        // Keep AllThingsTalk & WiFi connection alive
}
